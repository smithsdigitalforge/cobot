MODULE mMainModule_L
    CONST jointtarget jZero_L:=[[0,-130,30,0,40,0],[135,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST jointtarget jHome_L:=[[-20.4534,-122.853,33.735,59.6297,79.7369,58.7839],[111.551,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST jointtarget jNinja_L:=[[-13.4679,-135.622,-51.4128,28.4452,34.6338,-167.977],[136.989,9E+09,9E+09,9E+09,9E+09,9E+09]];
    !
    VAR cameratarget mycameratarget;
    VAR bool bLeadThrough_L:=FALSE;
    !
    PERS tasks taskYuMi{2}:=[["T_ROB_R"],["T_ROB_L"]];
    VAR syncident sync_Start;
    VAR syncident sync_Stop;
    PERS num instructionCode;
    !PERS num response;
    PERS bool connected;
 

 VAR robtarget l1;
 CONST jointtarget jNinja_L10:=[[-13.5293,-135.661,13.2042,-6.74649,43.3609,-167.977],[121.071,9E+09,9E+09,9E+09,9E+09,9E+09]];
 CONST jointtarget jNinja_L20:=[[-13.5687,-129.235,46.8528,-128.824,58.2251,-106.031],[-11.6521,9E+09,9E+09,9E+09,9E+09,9E+09]];
 CONST jointtarget jNinja_L30:=[[4.53131,-94.1292,-20.4553,-141.065,27.1064,-11.3332],[-34.556,9E+09,9E+09,9E+09,9E+09,9E+09]];
 
    PROC main12()
        mvDance_2_L;
    ENDPROC 
ENDMODULE