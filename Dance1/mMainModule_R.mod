MODULE mMainModule_R
    CONST jointtarget jZero_R:=[[0,-130,30,0,40,0],[-135,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST jointtarget jHome_R:=[[24.2401,-128.479,28.6464,-67.1574,85.7893,38.3464],[-107.33,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST jointtarget jNinja_R:=[[13.4679,-135.622,-51.4128,-28.4452,34.6338,-167.977],[-136.989,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget pVision_Wobj_Calib_R:=[[484.57,0,365],[9.38184E-06,0.706876,-0.707337,8.45457E-06],[1,0,-1,4],[-173.405,9E+09,9E+09,9E+09,9E+09,9E+09]];
    !
    VAR cameratarget mycameratarget;
    VAR bool bLeadThrough_R:=FALSE;
    !
    PERS tasks taskYuMi{2}:=[["T_ROB_R"],["T_ROB_L"]];
    VAR syncident sync_Start;
    VAR syncident sync_Stop;

    PERS num instructionCode;
    PERS bool connected;
    !//Client connected_R
    PERS num response;

    PROC main12()
    rDance_R;
    ENDPROC
ENDMODULE